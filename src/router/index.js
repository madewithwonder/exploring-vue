import Vue from 'vue';
import Router from 'vue-router';
import SimpleMessage from '@/components/SimpleMessage';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'SimpleMessage',
      component: SimpleMessage,
    },
  ],
});
